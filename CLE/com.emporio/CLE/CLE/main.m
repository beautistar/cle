//
//  main.m
//  CLE
//
//  Created by praveen on 3/15/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
