//
//  CLEBarCodeScanViewController.h
//  CLE
//
//  Created by praveen on 3/31/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "CLEBaseViewController.h"

@interface CLEBarCodeScanViewController : CLEBaseViewController
@property (strong, nonatomic) NSMutableArray * allowedBarcodeTypes;
@property (strong, nonatomic) NSMutableArray * foundBarcodes;
@property (weak, nonatomic) IBOutlet UIView *previewView;


@end
