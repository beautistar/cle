//
//  CLESignViewController.h
//  CLE
//
//  Created by praveen on 3/26/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#import "CLEBaseViewController.h"

@interface CLESignViewController : CLEBaseViewController

@property (weak, nonatomic) IBOutlet UITextField *ibUserId;
@property (weak, nonatomic) IBOutlet UITextField *ibPassword;

@end
