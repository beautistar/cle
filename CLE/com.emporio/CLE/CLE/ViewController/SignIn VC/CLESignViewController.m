//
//  CLESignViewController.m
//  CLE
//
//  Created by praveen on 3/26/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#import "CLESignViewController.h"
#import "CLEAPIRequest.h"
#import "XMLDictionary.h"
#import "CLEMainMenuViewController.h"

@interface CLESignViewController ()<CLEAPIRequestDelegate>

@end

@implementation CLESignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_ibUserId becomeFirstResponder];
}
-(IBAction)didTapOnLogInButton:(UIButton *)sender{
    [self.view endEditing:YES];
    NSString *userId = [NSString stringWithFormat:@"%@",_ibUserId.text];
    NSString *password = [NSString stringWithFormat:@"%@",_ibPassword.text];
    if ([userId length] > 20 || [userId isEqualToString:@""]) {
        [self showErrorMessage:@"Invalide User ID"];
        return;
    }
    if ([password length] > 30 || [password isEqualToString:@""]) {
           [self showErrorMessage:@"Invalide Password"];
        return;
    }
    NSDictionary *sendData = @{
                                @"userId":userId,
                                @"password":password
                               };
    [self showLoading];
    CLEAPIRequest *api = [CLEAPIRequest sharedInstance];
    api.delegate = self;
    [api signIn:sendData];
}

-(void)responseString:(NSString *)response{
    [self hideLoading];
    NSLog(@"response:%@",response);
    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:response];
    NSLog(@"xmlDictionary: %@",xmlDoc);
    NSDictionary *dic = [[xmlDoc objectForKey:@"Soap:Body"] objectForKey:@"GfncValidatePassword_Result"];
    NSString *resultValue= [dic objectForKey:@"return_value"];
    if ([resultValue isEqualToString:@"OK"]) {
          [self gotoNextViewController];
    }
    else{
         [self showErrorMessage:@"Login failed"];
    }
    
}

-(void)gotoNextViewController{
    CLEMainMenuViewController *main = [[CLEMainMenuViewController alloc]init];
    [self.navigationController pushViewController:main animated:YES];
}

 
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

@end
