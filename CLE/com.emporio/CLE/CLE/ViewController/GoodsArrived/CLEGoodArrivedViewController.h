//
//  CLEGoodArrivedViewController.h
//  CLE
//
//  Created by praveen on 3/26/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#import "CLEBaseViewController.h"

@interface CLEGoodArrivedViewController : CLEBaseViewController

@property (nonatomic, strong) IBOutlet UITextField *ibOrderId;

@property (nonatomic, strong) IBOutlet UILabel *ibDatExpectedReceiptDate;
@property (nonatomic, strong) IBOutlet UILabel *ibIntOutOfStockLines;
@property (nonatomic, strong) IBOutlet UILabel *ibIntPOLinesExpected;
@property (nonatomic, strong) IBOutlet UILabel *ibTxtRemarks;
@property (nonatomic, strong) IBOutlet UILabel *ibTxtSupplierName;
@property (nonatomic, strong) IBOutlet UILabel *ibTxtDebugConnecationStatus;
@property (nonatomic, strong) IBOutlet UILabel *ibTxtDebugResultString;
@property (nonatomic, strong) NSString *barCodeForOrderId;
@property (nonatomic, strong) IBOutlet UIButton *checkButton;
@end
