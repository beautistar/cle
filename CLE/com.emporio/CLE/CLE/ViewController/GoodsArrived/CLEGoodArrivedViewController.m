//
//  CLEGoodArrivedViewController.m
//  CLE
//
//  Created by praveen on 3/26/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#import "CLEGoodArrivedViewController.h"
#import "CLEBarCodeScanViewController.h"
#import "CLEAPIRequest.h"
#import "XMLDictionary.h"
#import "DTDevices.h"

@interface CLEGoodArrivedViewController ()<CLEAPIRequestDelegate, DTDeviceDelegate>{
    DTDevices *scanner;
}

@end

@implementation CLEGoodArrivedViewController

#pragma mark - UIView-Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Goods Arrived";
    
    //DTDevice init
    scanner=[DTDevices sharedDevice];
    [scanner addDelegate:self];
    [scanner connect];
    
    //KeyBoard
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    _ibOrderId.inputView = dummyView;
    [self.ibOrderId becomeFirstResponder];
    
    [_ibOrderId addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    [_ibOrderId addTarget:self
                   action:@selector(callApiDoneWithReturnKey)
         forControlEvents:UIControlEventEditingDidEndOnExit];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    NSLog(@"scanned barcode : %@", _barCodeForOrderId);
    if (_barCodeForOrderId) {
        [self.view endEditing:YES];
        _ibOrderId.text = _barCodeForOrderId;
        [self checkOrderIdArrived:_ibOrderId.text];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}

#pragma mark - DTDevices-Delegate Methods
-(void)barcodeData:(NSString *)barcode type:(int)type {
    NSLog(@"barcodeData:%@ type:%d",barcode,type);
    
    //Testing Labels Are added
    _ibTxtDebugResultString.text = [NSString stringWithFormat:@"Barcode: %@",barcode];
    
    if (![barcode isEqualToString:@""]) {
        self.checkButton.hidden = YES;
        _ibOrderId.text = barcode;
        [self checkOrderIdArrived:barcode];
    }
}

-(void)connectionState:(int)state {
    switch (state) {
        case CONN_DISCONNECTED:
            //Disconnected
            _ibTxtDebugConnecationStatus.text = @"DisConnected";
            break;
        case CONN_CONNECTING:
            //Connecting
            _ibTxtDebugConnecationStatus.text = @"Connecting";
            break;
        case CONN_CONNECTED:
            //Connected
            _ibTxtDebugConnecationStatus.text = @"Connected";
            break;
    }
}

#pragma mark - UItextFields-Delegate Methods
-(void)callApiDoneWithReturnKey{
    NSString *orderId = _ibOrderId.text;
    if (![orderId isEqualToString:@""]) {
            [self checkOrderIdArrived:orderId];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidChange:(UITextField *)theTextField{
    _ibDatExpectedReceiptDate.text = @"------------";
    _ibIntOutOfStockLines.text = @"------------";
    _ibIntPOLinesExpected.text = @"------------";
    _ibTxtSupplierName.text = @"------------";
    _ibTxtRemarks.text = @"------------";
}

#pragma mark - UIButton-IBAction Methods
-(IBAction)didTapOnScanBarCode:(UIButton *)sender{
    [self textFieldDidChange:_ibOrderId];
    _ibOrderId.text = nil;
    _barCodeForOrderId = nil;
    CLEBarCodeScanViewController *barcodescanVC = [[CLEBarCodeScanViewController alloc]init];
    [self.navigationController pushViewController:barcodescanVC animated:YES];
}

-(IBAction)dudTapCheckButton:(UIButton *)sender{
    [self.view endEditing:YES];
    NSString *orderId = _ibOrderId.text;
    if ([orderId isEqualToString:@""]) {
        [self showErrorMessage:@"Please Enter Order ID"];
        return;
    }
    [self checkOrderIdArrived:orderId];
}

#pragma mark - CLE-Api Methods
-(void)checkOrderIdArrived:(NSString *)orderId{
    [self showLoading];
    CLEAPIRequest *api = [CLEAPIRequest sharedInstance];
    api.delegate = self;
    [api getOrderDetailsByOrderID:orderId];
}
- (void)responseString:(NSString *)responseData{
    [self hideLoading];
    self.checkButton.hidden = NO;
    self.ibOrderId.text = nil;
    if (responseData == nil) {
        [self showErrorMessage:@"Internet Connecation Failed!!!"];
        return;
    }
    NSLog(@"response:%@",responseData);
    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:responseData];
    NSLog(@"xmlDictionary: %@",xmlDoc);
    NSDictionary *dic = [[xmlDoc objectForKey:@"Soap:Body"] objectForKey:@"GfncGoodsArrivedOnSite_Result"];
    
    if ([[dic objectForKey:@"p_txtRemarks"] rangeOfString:@"not found."].location == NSNotFound) {
     
        _ibIntOutOfStockLines.text = [dic objectForKey:@"p_intOutOfStockLines"];
        _ibIntPOLinesExpected.text = [dic objectForKey:@"p_intPOLinesExpected"];
        _ibTxtSupplierName.text = [dic objectForKey:@"p_txtSupplierName"];
        NSArray *remars = [[dic objectForKey:@"p_txtRemarks"] componentsSeparatedByString:@":"];
        if (remars.count > 2) {
            _ibTxtRemarks.text = [NSString stringWithFormat:@"%@:%@",remars[1],remars[2]];
        }else{
            _ibTxtRemarks.text = [NSString stringWithFormat:@"%@",remars[1]];
        }
        
        
        NSString *expectedReceiptDate =  [dic objectForKey:@"p_datExpectedReceiptDate"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormatter dateFromString:expectedReceiptDate];
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSString *convertedString = [dateFormatter stringFromDate:date];
        _ibDatExpectedReceiptDate.text = convertedString;
        
    } else {
        _ibDatExpectedReceiptDate.text = @"------------";
        _ibIntOutOfStockLines.text = @"------------";
        _ibIntPOLinesExpected.text = @"------------";
        _ibTxtSupplierName.text = @"------------";
        _ibTxtRemarks.text = [NSString stringWithFormat:@"Order %@ not found.",_ibOrderId.text];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


@end
