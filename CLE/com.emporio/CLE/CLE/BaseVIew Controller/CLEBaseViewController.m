//
//  CLEBaseViewController.m
//  CLE
//
//  Created by praveen on 3/26/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#import "CLEBaseViewController.h"

@interface CLEBaseViewController ()<MBProgressHUDDelegate>

@end

@implementation CLEBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _progress.delegate = self;
}
#pragma - MBProgressBar
-(void)showLoading{
    _progress = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    _progress.label.text = @"Loading";
}
-(void)hideLoading{
    [_progress hideAnimated:YES];
}
-(void)showErrorMessage:(NSString *)message{
    _progress   = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    _progress.mode = MBProgressHUDModeText;
    _progress.detailsLabel.text = NSLocalizedString(message, @"HUD message title");
    // Move to bottm center.
    _progress.offset = CGPointMake(0.f, MBProgressMaxOffset);
    [_progress hideAnimated:YES afterDelay:3.f];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
