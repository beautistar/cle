//
//  CLEBaseViewController.h
//  CLE
//
//  Created by praveen on 3/26/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface CLEBaseViewController : UIViewController

@property MBProgressHUD *progress;
-(void)showLoading;
-(void)hideLoading;
-(void)showErrorMessage:(NSString *)message;
@end
