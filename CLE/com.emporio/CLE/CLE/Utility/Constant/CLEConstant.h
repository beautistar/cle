//
//  CLEConstant.h
//  CLE
//
//  Created by praveen on 3/26/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#ifndef CLEConstant_h
#define CLEConstant_h

#define kDomain @"clfdistribution"

// test server
#define kUserName @"appwebservices"
#define kPassword @"AwSiOs@10"
#define kBaseUrl @"http://87.224.121.66:7049/dynamicsNAV/ws/CLF%20Distribution%20Live/Codeunit/WarehouseManagement"

/* live server
#define kUserName @"NavWebServices"
#define kPassword @"C15emp3H"
#define kBaseUrl @"http://remote.clfdistribution.com:7047/dynamicsNAV/ws/CLF%20Distribution%20Live/Codeunit/WarehouseManagement"
*/
#endif /* CLEConstant_h */
