//
//  CLEAPIRequest.m
//  CLE
//
//  Created by praveen on 3/26/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#import "CLEAPIRequest.h"
#import "CLEConstant.h"

@implementation CLEAPIRequest

+ (instancetype)sharedInstance
{
    static CLEAPIRequest *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CLEAPIRequest alloc] init];
    });
    return sharedInstance;
}

-(void)signIn:(NSDictionary*)userData{
    NSString  *soapBoady = [self getHttpBoady:userData];
    NSString *soapAction = [self getSaopAction:@"GfncValidatePassword"];
    NSLog(@"REQUEST PARAMETER:  %@",soapBoady);
    [self sendDataToServer:soapBoady soapAction:soapAction];
}
-(void)getOrderDetailsByOrderID:(NSString *)orderId{
    NSString  *soapBoady = [self getHttpBoadyForOrderId:orderId];
    NSString *soapAction = [self getSaopAction:@"GfncGoodsArrivedOnSite"];  //GfncCheckGoodsArrived
        NSLog(@"REQUEST PARAMETER:  %@",soapBoady);
    [self sendDataToServer:soapBoady soapAction:soapAction];
}

-(void)sendDataToServer:(NSString *)soapBoady soapAction:(NSString *)soapAction{
    NSString  *authStr = [self getAuthorization];
    
    // for test url
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    // for live url
    //NSURL *url = [NSURL URLWithString:kBaseLiveUrl];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapBoady length]];
    [theRequest addValue:@"text/xml;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue:soapAction forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapBoady dataUsingEncoding:NSUTF8StringEncoding]];
    theRequest.timeoutInterval = 20;
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [theRequest setValue:authValue forHTTPHeaderField:@"Authorization"];
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    [theConnection start];
}

-(NSString *)getAuthorization{
    // for test url
    return  [NSString stringWithFormat:@"%@:%@:%@",kUserName,kPassword,kPassword];
    // for live url user
    //return  [NSString stringWithFormat:@"%@:%@:%@",kUserNameLive,kPasswordLive,kPasswordLive];
}

-(NSString *)getHttpBoadyForOrderId:(NSString *)orderId{
    NSString *soapBoady = [NSString stringWithFormat:@"<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n"
                           "<soap:Body>\n"
                           "<GfncGoodsArrivedOnSite xmlns=\"urn:microsoft-dynamics-schemas/codeunit/WarehouseManagement\">\n"
                           "<p_codDocumentNo>%@</p_codDocumentNo>\n"
                             "<p_txtSupplierName></p_txtSupplierName>\n"
                             "<p_datExpectedReceiptDate>2000-01-01</p_datExpectedReceiptDate>\n"
                             "<p_intPOLinesExpected>0</p_intPOLinesExpected>\n"
                             "<p_intOutOfStockLines>0</p_intOutOfStockLines>\n"
                             "<p_txtRemarks></p_txtRemarks>\n"
                           "</GfncGoodsArrivedOnSite>\n" //GfncGoodsArrivedOnSite  p_txtRemarks
                           "</soap:Body>\n"
                           "</soap:Envelope>",orderId];
    return soapBoady;
}

-(NSString *)getHttpBoady:(NSDictionary *)userData{
    NSString *userId = [userData objectForKey:@"userId"];
    NSString *password = [userData objectForKey:@"password"];
    NSString *soapBoady = [NSString stringWithFormat:@"<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n"
                           "<soap:Body>\n"
                           "<GfncValidatePassword xmlns=\"urn:microsoft-dynamics-schemas/codeunit/WarehouseManagement\">\n"
                           "<p_codUserID>%@</p_codUserID>\n"
                           "<p_txtPassword>%@</p_txtPassword>\n"
                           "<p_codLocationCode>210</p_codLocationCode>\n"
                           "</GfncValidatePassword>\n"
                           "</soap:Body>\n"
                           "</soap:Envelope>",userId,password];
    return soapBoady;
}
-(NSString *)getSaopAction:(NSString *)MethodName{
    NSString *soapAction = [NSString stringWithFormat: @"urn:microsoft-dynamics-schemas/codeunit/WarehouseManagement:%@",MethodName];
    return  soapAction;
}
- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection{
    return YES;
}
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return YES;
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    NSURLCredential *newCredential;
    newCredential = [NSURLCredential credentialWithUser:kUserName
                                               password:kPassword
                                            persistence:NSURLCredentialPersistenceNone];
    NSLog(@"NEwCred:- %@ %@", newCredential, newCredential);
    [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
}
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    NSURLCredential *newCredential;
    newCredential = [NSURLCredential credentialWithUser:kUserName
                                               password:kPassword
                                            persistence:NSURLCredentialPersistenceNone];
    NSLog(@"NEwCred:- %@ %@", newCredential, newCredential);
    [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"error:%@",error);
     [self.delegate responseString:nil];
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    NSLog(@"response:%@",response);
    //NSHTTPURLResponse *aResponse = (NSHTTPURLResponse *)response;
    //NSInteger statusCodeResponse = aResponse.statusCode;
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"data:%@",data);
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    [self.delegate responseString:responseString];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSLog(@"connection:%@",connection);
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(nullable NSError *)error{
    NSLog(@"didBecomeInvalidWithError:%@",error);
    
}
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler{
    completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
    NSLog(@"NSURLSessionAuthChallengeDisposition");
}

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session{
    NSLog(@"URLSessionDidFinishEventsForBackgroundURLSession");
}


@end
