//
//  CLEAPIRequest.h
//  CLE
//
//  Created by praveen on 3/26/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CLEAPIRequest;
@protocol CLEAPIRequestDelegate <NSObject>
- (void)responseString:(NSString *)responseData;
@end //end protocol


@interface CLEAPIRequest : NSObject
+ (instancetype)sharedInstance;
@property (nonatomic, weak) id <CLEAPIRequestDelegate> delegate;
-(void)signIn:(NSDictionary*)userData;
-(void)getOrderDetailsByOrderID:(NSString *)orderId;

@end
