//
//  AppDelegate.h
//  CLE
//
//  Created by praveen on 3/15/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

