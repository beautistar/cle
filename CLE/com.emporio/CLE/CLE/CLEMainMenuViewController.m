//
//  CLEMainMenuViewController.m
//  CLE
//
//  Created by praveen on 3/26/17.
//  Copyright © 2017 com.emporio. All rights reserved.
//

#import "CLEMainMenuViewController.h"
#import "CLEGoodArrivedViewController.h"

@interface CLEMainMenuViewController ()

@end

@implementation CLEMainMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)didTaponGoodsArrived:(UIButton *)sender{
    CLEGoodArrivedViewController *goodsArrivedVC = [[CLEGoodArrivedViewController alloc]init];
    [self.navigationController pushViewController:goodsArrivedVC animated:YES];
    
}
-(IBAction)didTaponPutWay:(UIButton *)sender{
    
}
-(IBAction)didTaponPick:(UIButton *)sender{
    
}
-(IBAction)didTaponMove:(UIButton *)sender{
    
}
-(IBAction)didTaponLogOut:(UIButton *)sender{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 

@end
